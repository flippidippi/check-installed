const { spawn } = require('child_process')
const semver = require('semver')
const { OPTIONS } = require('./constants')

/**
 * Get the version of an engine
 * @param {string} name - The name of the engine to get version for
 * @returns {Promise<string>} - The version of the engine
 */
async function getEngineVersion (name) {
  const spawned = spawn(name, ['--version'], { shell: true })

  return new Promise((resolve) => {
    let result = ''

    spawned.stdout.on('data', (data) => {
      result += data.toString()
    })

    spawned.on('close', (code) => {
      if (code !== 0) {
        resolve('none')
      } else {
        resolve(result.trim())
      }
    })
  })
}

/**
 * Checks that the given engine is installed and satisfies version (throws if not)
 * @param {string} name - The name of the engine to check
 * @param {string} version - The version to check against
 * @returns {Promise<[string, string, string]>} - The [name, version, installed] installed and checked successfully
 */
async function checkEngineVersion (name, version) {
  const installed = await getEngineVersion(name)

  if (!semver.satisfies(installed, version)) {
    throw new Error(`check-installed engine failed for ${name}: installed ${installed}, expected ${version}`)
  }

  return [name, version, installed]
}

/**
 * Checks engines in `package.json` are installed
 * @param {object} json - The `package.json` JSON object
 * @param {Partial<typeof import('./constants').OPTIONS>} options - Options for the CLI
 * @returns {Promise<void>} - Resolves when all engines are checked (throws on check fail)
 */
async function checkEngines (json = {}, options = {}) {
  const { showSuccess, showEngines } = { ...OPTIONS, ...options }

  // Check engines
  const engines = await Promise.all(
    Object.entries(json.engines || {})
      .map(([name, version]) => checkEngineVersion(name, version))
  )

  if (showSuccess) {
    console.log('check-installed engines successful')
  }

  if (showEngines) {
    for (const [name, version, installed] of engines) {
      console.log(`${name}: ${installed} (${version})`)
    }
  }

  if (showSuccess || showEngines) {
    console.log('')
  }
}

module.exports = {
  checkEngines
}
