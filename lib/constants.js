/**
 * Help text for the CLI
 */
const HELP = `
Usage: check-installed [options]

Options:
  --help          Show help (skips check)
  --skip-engines  Skip checking engines
  --skip-modules  Skip checking node modules
  --show-success  Show success message on successful checks
  --show-engines  Show installed engines on successful check
  --show-modules  Show installed node modules on successful check

See https://www.npmjs.com/package/check-installed
`.trim()

/**
 * Default options for the CLI
 */
const OPTIONS = {
  help: false,
  skipEngines: false,
  skipModules: false,
  showSuccess: false,
  showEngines: false,
  showModules: false
}

module.exports = {
  HELP,
  OPTIONS
}
