const { resolve } = require('path')
const { HELP } = require('./constants')
const { getOptions } = require('./getOptions')
const { checkEngines } = require('./checkEngines')
const { checkModules } = require('./checkModules')

/**
 * check-installed CLI entry point
 * @param {string[]} argv - CLI arguments
 * @returns {Promise<void>} - Promise that resolves when the CLI is done (throws on check fail)
 */
async function cli (argv = process.argv) {
  const json = require(resolve(process.cwd(), 'package.json'))
  const options = getOptions(argv.slice(2))

  if (options.help) {
    console.log(HELP)
    return
  }

  if (!options.skipEngines) {
    await checkEngines(json, options)
  }

  if (!options.skipModules) {
    await checkModules(json, options)
  }
}

module.exports = {
  cli
}
