const { OPTIONS } = require('./constants')

/**
 * Get check-installed options from CLI arguments
 * @param {string[]} args - CLI arguments
 * @returns {Partial<typeof OPTIONS>} - Options for the CLI with defaults
 */
function getOptions (args = []) {
  const options = {}

  for (const arg of args) {
    if (arg.startsWith('--')) {
      const [key, value] = arg.slice(2).split('=')
      const keyCamelCase = key.replace(/-([a-z])/g, (_, c) => c.toUpperCase())
      const option = OPTIONS[keyCamelCase]

      // Only set if the option is defined in `OPTIONS`
      if (option !== undefined) {
        let newValue

        // If boolean, `true` only if no value or value is "true"
        // Otherwise, only if value provided
        if (typeof option === 'boolean') {
          newValue = value == null ? true : value === 'true'
        } else if (value != null) {
          newValue = value
        }

        // Set only if valid and not already default
        if (newValue !== undefined && newValue !== option) {
          options[keyCamelCase] = newValue
        }
      }
    }
  }

  return options
}

module.exports = {
  getOptions
}
