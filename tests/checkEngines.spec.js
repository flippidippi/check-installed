const { spawn } = require('child_process')
const { checkEngines } = require('..')

jest.mock('child_process')

/**
 * Creates a `package.json` object with the given engines
 * @param {Record<string, string>} engines - Map of wanted engine versions
 * @returns {{ engines: Record<string, string> }} - The `package.json` JSON object
 */
function getJson (engines) {
  return { engines }
}

/**
 * Mocks `spawn` version checks as successful for given installed map
 * @param {Record<string, string | Error>} engines - Map of installed engine versions
 * @returns {void}
 */
function mockInstalled (engines) {
  // @ts-ignore
  spawn.mockImplementation((name) => {
    const installed = engines[name]

    return {
      stdout: {
        on: (event, callback) => {
          if (event === 'data' && typeof installed === 'string') {
            callback(installed)
          }
        }
      },
      on: (event, callback) => {
        if (installed === undefined || typeof installed === 'object') {
          /* eslint-disable-next-line n/no-callback-literal */
          callback(1)
        } else {
          /* eslint-disable-next-line n/no-callback-literal */
          callback(0)
        }
      }
    }
  })
}

describe('checkEngines', () => {
  afterEach(() => {
    jest.clearAllMocks()
    jest.resetAllMocks()
  })

  it('checks no json successfully', async () => {
    await expect(checkEngines()).resolves.not.toThrow()
  })

  it('checks no engines successfully', async () => {
    await expect(checkEngines({})).resolves.not.toThrow()
  })

  it('checks empty engines successfully', async () => {
    const json = getJson({})

    await expect(checkEngines(json)).resolves.not.toThrow()
  })

  it('checks single engine successfully', async () => {
    const json = getJson({
      node: '>=12'
    })
    mockInstalled({
      node: 'v18.0.0'
    })

    await expect(checkEngines(json)).resolves.not.toThrow()
  })

  it('checks multiple engines successfully', async () => {
    const json = getJson({
      node: '12',
      npm: '^6.0.0',
      dotnet: '6'
    })
    mockInstalled({
      node: 'v12.1.1',
      npm: '6.1.1',
      dotnet: '6.0.414'
    })

    await expect(checkEngines(json)).resolves.not.toThrow()
  })

  it('shows success', async () => {
    const options = { showSuccess: true }
    const json = getJson({
      node: '>=12'
    })
    mockInstalled({
      node: 'v18.0.0'
    })
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(checkEngines(json, options)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith('check-installed engines successful')
  })

  it('shows engines', async () => {
    const options = { showEngines: true }
    const json = getJson({
      node: '18',
      npm: '>=6'
    })
    mockInstalled({
      node: '18.0.0',
      npm: '9.0.0'
    })
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(checkEngines(json, options)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith('node: 18.0.0 (18)')
    expect(console.log).toHaveBeenCalledWith('npm: 9.0.0 (>=6)')
  })

  it('throws if wrong version', async () => {
    const json = getJson({
      node: '12',
      npm: '^6.0.0',
      dotnet: '6'
    })
    mockInstalled({
      node: 'v12.1.1',
      npm: '5.1.1',
      dotnet: '5.0.414'
    })

    await expect(checkEngines(json)).rejects.toThrow('check-installed engine failed for npm: installed 5.1.1, expected ^6.0.0')
  })

  it('throws if not installed', async () => {
    const json = getJson({
      node: '12',
      npm: '^6.0.0',
      dotnet: '6'
    })
    mockInstalled({
      node: 'v12.1.1',
      dotnet: '5.0.414'
    })
    jest.spyOn(console, 'error').mockImplementation(() => {})

    await expect(checkEngines(json)).rejects.toThrow('check-installed engine failed for npm: installed none, expected ^6.0.0')
  })

  it('throws on spawn error', async () => {
    const json = getJson({
      node: '12',
      npm: '^6.0.0',
      dotnet: '6'
    })
    mockInstalled({
      node: 'v12.1.1',
      npm: new Error('error log'),
      dotnet: '5.0.414'
    })
    jest.spyOn(console, 'error').mockImplementation(() => {})

    await expect(checkEngines(json)).rejects.toThrow('check-installed engine failed for npm: installed none, expected ^6.0.0')
  })
})
