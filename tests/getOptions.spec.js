const { getOptions } = require('../lib/getOptions')

jest.mock('../lib/constants', () => ({
  OPTIONS: {
    lowercase: false,
    booleanFalse: false,
    booleanTrue: true,
    stringBlank: '',
    stringNull: null
  }
}))

describe('getOptions', () => {
  it('handles no arguments', () => {
    expect(getOptions()).toEqual({})
  })

  it('keeps lowercase argument lowercase', () => {
    expect(getOptions(['--lowercase'])).toEqual({
      lowercase: true
    })
  })

  it('converts kebab-case argument to camelCase', () => {
    expect(getOptions(['--boolean-false'])).toEqual({
      booleanFalse: true
    })
  })

  it('keeps camelCase argument camelCase', () => {
    expect(getOptions(['--booleanFalse'])).toEqual({
      booleanFalse: true
    })
  })

  it('handles multiple arguments', () => {
    expect(getOptions(['--boolean-false', '--boolean-true=false'])).toEqual({
      booleanFalse: true,
      booleanTrue: false
    })
  })

  it.each([
    ['', {}],
    ['--boolean-false', { booleanFalse: true }],
    ['--boolean-false=', {}],
    ['--boolean-false=true', { booleanFalse: true }],
    ['--boolean-false=false', {}],
    ['--boolean-false=other', {}]
  ])('handles boolean default false with \'%s\'', (arg, expected) => {
    expect(getOptions([arg])).toEqual(expected)
  })

  it.each([
    ['', {}],
    ['--boolean-true', {}],
    ['--boolean-true=', { booleanTrue: false }],
    ['--boolean-true=true', {}],
    ['--boolean-true=false', { booleanTrue: false }],
    ['--boolean-true=other', { booleanTrue: false }]
  ])('handles boolean default true with \'%s\'', (arg, expected) => {
    expect(getOptions([arg])).toEqual(expected)
  })

  it.each([
    ['', {}],
    ['--string-blank', {}],
    ['--string-blank=', {}],
    ['--string-blank=something', { stringBlank: 'something' }]
  ])('handles string default \'\' with \'%s\'', (arg, expected) => {
    expect(getOptions([arg])).toEqual(expected)
  })

  it.each([
    ['', {}],
    ['--string-null', {}],
    ['--string-null=', { stringNull: '' }],
    ['--string-null=something', { stringNull: 'something' }]
  ])('handles string default null with \'%s\'', (arg, expected) => {
    expect(getOptions([arg])).toEqual(expected)
  })

  it('ignores invalid arguments', () => {
    expect(getOptions(['--invalid-arg'])).toEqual({})
  })
})
