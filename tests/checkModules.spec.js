const { resolve } = require('path')
const fs = require('fs')
const { checkModules } = require('../lib/checkModules')

jest.mock('fs', () => ({ existsSync: jest.fn() }))

/**
 * Creates a `package.json` object with the given modules
 * @param {Partial<{
 *   dependencies: Record<string, string>,
 *   devDependencies: Record<string, string>,
 *   optionalDependencies: Record<string, string>
 *  }>} dependencies: The dependencies for `package.json`
 * @return {Partial<{
 *   dependencies: Record<string, string>,
 *   devDependencies: Record<string, string>,
 *   optionalDependencies: Record<string, string>
 *  }>}: The dependencies for `package.json`
 */
function getJson (dependencies = {}) {
  return { ...dependencies }
}

/**
 * Mocks a module `package.json` version
 * @param {Record<string, string>} modules - Map of module versions to mock install
 */
function mockModules (modules) {
  const paths = []
  for (const [name, version] of Object.entries(modules)) {
    const path = resolve('node_modules', name, 'package.json')
    paths.push(path)
    jest.doMock(path, () => ({ version }), { virtual: true })
  }
  // @ts-ignore
  fs.existsSync.mockImplementation((path) => paths.includes(path))
}

describe('checkModules', () => {
  afterEach(() => {
    jest.clearAllMocks()
    jest.resetAllMocks()
    jest.resetModules()
  })

  it('checks no json successfully', async () => {
    await expect(checkModules()).resolves.not.toThrow()
  })

  it('checks no modules successfully', async () => {
    await expect(checkModules({})).resolves.not.toThrow()
  })

  it('checks empty modules successfully', async () => {
    const json = getJson({
      dependencies: {},
      devDependencies: {},
      optionalDependencies: {}
    })

    await expect(checkModules(json)).resolves.not.toThrow()
  })

  it.each([
    ['dependencies'],
    ['devDependencies'],
    ['optionalDependencies']
  ])('checks single %s successfully', async (name) => {
    const json = getJson({
      [name]: {
        module1: '1.0.0'
      }
    })
    mockModules({
      module1: '1.0.0'
    })

    await expect(checkModules(json)).resolves.not.toThrow()
  })

  it('checks multiple modules successfully', async () => {
    const json = getJson({
      dependencies: {
        module1: '1.0.1',
        module2: '1.0.2'
      },
      devDependencies: {
        '@mod/module3': '1.0.3'
      },
      optionalDependencies: {
        module4: '1.0.4'
      }
    })
    mockModules({
      module1: '1.0.1',
      module2: '1.0.2',
      '@mod/module3': '1.0.3',
      module4: '1.0.4'
    })

    await expect(checkModules(json)).resolves.not.toThrow()
  })

  it('shows success', async () => {
    const options = { showSuccess: true }
    const json = getJson({
      dependencies: {
        module1: '1.0.0',
        module2: '>=1'
      }
    })
    mockModules({
      module1: '1.0.0',
      module2: '2.0.0'
    })
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(checkModules(json, options)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith('check-installed modules successful')
  })

  it('shows modules', async () => {
    const options = { showModules: true }
    const json = getJson({
      dependencies: {
        module1: '>1.0.0 <2.0.0',
        module2: '>=1'
      },
      devDependencies: {
        module3: '^6.0.0',
        module4: 'latest'
      },
      optionalDependencies: {
        module5: '1.2.3',
        module6: '3.2.1'
      }
    })
    mockModules({
      module1: '1.5.0',
      module2: '2.0.0',
      module3: '6.5.4',
      module4: '1.1.1',
      module5: '1.2.3'
    })
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(checkModules(json, options)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith('module1: 1.5.0 (>1.0.0 <2.0.0)')
    expect(console.log).toHaveBeenCalledWith('module2: 2.0.0 (>=1)')
    expect(console.log).toHaveBeenCalledWith('module3: 6.5.4 (^6.0.0)')
    expect(console.log).toHaveBeenCalledWith('module4: 1.1.1 (latest)')
    expect(console.log).toHaveBeenCalledWith('module5: 1.2.3 (1.2.3)')
    expect(console.log).toHaveBeenCalledWith('module6: none (3.2.1)')
  })

  it.each([
    ['dependencies'],
    ['devDependencies'],
    ['optionalDependencies']
  ])('throws %s wrong version', async (name) => {
    const json = getJson({
      [name]: {
        module1: '~1.0.0'
      }
    })
    mockModules({
      module1: '1.1.1'
    })

    await expect(checkModules(json)).rejects.toThrow('installed 1.1.1')
  })

  it.each([
    ['dependencies'],
    ['devDependencies']
  ])('throws %s not installed', async (name) => {
    const json = getJson({
      [name]: {
        module1: '~1.0.0'
      }
    })

    await expect(checkModules(json)).rejects.toThrow('installed none')
  })

  it('checks optionalDependencies not installed successfully', async () => {
    const json = getJson({
      optionalDependencies: {
        module1: '~1.0.0'
      }
    })

    await expect(checkModules(json)).resolves.not.toThrow()
  })

  it.each([
    ['dependencies'],
    ['devDependencies'],
    ['optionalDependencies']
  ])('checks latest %s installed successfully', async (name) => {
    const json = getJson({
      [name]: {
        module1: 'latest'
      }
    })
    mockModules({
      module1: '1.0.1'
    })

    await expect(checkModules(json)).resolves.not.toThrow()
  })

  it.each([
    ['dependencies'],
    ['devDependencies']
  ])('throws latest %s not installed', async (name) => {
    const json = getJson({
      [name]: {
        mod1: 'latest'
      }
    })
    mockModules({
      module1: '1.0.1'
    })

    await expect(checkModules(json)).rejects.toThrow()
  })

  it.each([
    ['dependencies'],
    ['devDependencies'],
    ['optionalDependencies']
  ])('checks alias %s successfully', async (name) => {
    const json = getJson({
      [name]: {
        mod1: 'npm:module1@1.0.1'
      }
    })
    mockModules({
      mod1: '1.0.1'
    })

    await expect(checkModules(json)).resolves.not.toThrow()
  })

  it.each([
    ['dependencies'],
    ['devDependencies'],
    ['optionalDependencies']
  ])('throws alias %s wrong version', async (name) => {
    const json = getJson({
      [name]: {
        mod1: 'npm:module1@1.0.1'
      }
    })
    mockModules({
      mod1: '1.0.2'
    })

    await expect(checkModules(json)).rejects.toThrow()
  })

  it.each([
    ['dependencies'],
    ['devDependencies']
  ])('throws alias %s not installed', async (name) => {
    const json = getJson({
      [name]: {
        mod1: 'npm:module1@1.0.1'
      }
    })
    mockModules({
      module1: '1.0.1'
    })

    await expect(checkModules(json)).rejects.toThrow()
  })

  it('shows modules for alias', async () => {
    const options = { showModules: true }
    const json = getJson({
      dependencies: {
        mod1: 'npm:module1@1.1.1'
      }
    })
    mockModules({
      mod1: '1.1.1'
    })
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(checkModules(json, options)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith('mod1: 1.1.1 (module1 1.1.1)')
  })

  it.each([
    ['folder', 'file:cli'],
    ['file tar', 'file:cli.tar'],
    ['file tar.gz', 'file:cli.tar.gz'],
    ['file tgz', 'file:cli.tgz'],
    ['url http', 'http://github.com/npm/cli/archive/refs/tags/v9.9.0.tar.gz'],
    ['url https', 'https://github.com/npm/cli/archive/refs/tags/v9.9.0.tar.gz'],
    ['git', 'git://github.com/npm/cli.git'],
    ['git ssh', 'git+ssh://isaacs@github.com/npm/cli.git#v1.0.27'],
    ['git http', 'git+http://isaacs@github.com/npm/cli.git#semver:^6.0'],
    ['git https', 'git+https://isaacs@github.com/npm/cli.git'],
    ['git file', 'git+file://path/to/npm/cli'],
    ['github', 'github:npm/cli'],
    ['gitlab', 'gitlab:npm/cli#semver:^6.0'],
    ['bitbucket', 'bitbucket:npm/cli#v1.0.27'],
    ['gist', 'gist:0bce1161cfd2aa91ae7cad9abb42c342']
  ])('skips %s', async (name, version) => {
    const options = { showModules: true }
    const json = getJson({
      dependencies: {
        module1: version
      }
    })
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(checkModules(json, options)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith(`module1: skipped (${version})`)
  })
})
