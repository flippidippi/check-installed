const { cli } = require('../lib/cli')
const { checkEngines } = require('../lib/checkEngines')
const { checkModules } = require('../lib/checkModules')

const json = { version: '1.0.0' }
jest.mock('../package.json', () => json)
jest.mock('../lib/checkEngines', () => ({ checkEngines: jest.fn() }))
jest.mock('../lib/checkModules', () => ({ checkModules: jest.fn() }))

/**
 * Get `process.argv`-like array with CLI arguments
 * @param {string[]} args - CLI arguments
 * @returns {string[]} - `process.argv`-like array with CLI arguments
 */
function getArgv (args = []) {
  return ['', '', ...args]
}

describe('cli', () => {
  afterEach(() => {
    jest.clearAllMocks()
    jest.resetAllMocks()
  })

  it('runs checks', async () => {
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(cli()).resolves.not.toThrow()
    expect(console.log).not.toHaveBeenCalled()
    expect(checkEngines).toHaveBeenCalledWith(json, {})
    expect(checkModules).toHaveBeenCalledWith(json, {})
  })

  it('shows help', async () => {
    const argv = getArgv(['--help'])
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(cli(argv)).resolves.not.toThrow()
    expect(console.log).toHaveBeenCalledWith(expect.stringContaining('Usage: check-installed [options]'))
    expect(checkEngines).not.toHaveBeenCalled()
    expect(checkModules).not.toHaveBeenCalled()
  })

  it('skips check engines', async () => {
    const argv = getArgv(['--skip-engines'])
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(cli(argv)).resolves.not.toThrow()
    expect(console.log).not.toHaveBeenCalled()
    expect(checkEngines).not.toHaveBeenCalled()
    expect(checkModules).toHaveBeenCalledWith(json, { skipEngines: true })
  })

  it('skips check modules', async () => {
    const argv = getArgv(['--skip-modules'])
    jest.spyOn(console, 'log').mockImplementation(() => {})

    await expect(cli(argv)).resolves.not.toThrow()
    expect(console.log).not.toHaveBeenCalled()
    expect(checkEngines).toHaveBeenCalledWith(json, { skipModules: true })
    expect(checkModules).not.toHaveBeenCalled()
  })
})
