# check-installed
Minimal Node package to check that dependencies are installed ✅

[![npm](https://img.shields.io/npm/v/check-installed)](https://www.npmjs.com/package/check-installed)
[![pipeline status](https://gitlab.com/flippidippi/check-installed/badges/main/pipeline.svg)](https://gitlab.com/flippidippi/check-installed/-/pipelines?ref=main)
[![coverage report](https://gitlab.com/flippidippi/check-installed/badges/main/coverage.svg)](https://check-installed-flippidippi-09a6e441a6dd0a7a65f32f5fe3ec449245f.gitlab.io/coverage)

## Install
```bash
$ npm i check-installed --save-dev
```

## Checks
### Engines Check
- Compares the installed version of engines to the versions specified in `package.json`
- Engines can be things like node, dotnet, or anything that responds to `--version`
- Versions are checked with [semver](https://www.npmjs.com/package/semver), so you can use any valid semver range
```json
  "engines": {
    "node": ">=12.x",
    "npm": ">=6",
    "dotnet": "^6.0.410"
  },
```

### Modules Check
- Compares the installed versions of node modules to the versions specified in `package.json`
- Versions for modules that have `latest` are not checked, only that they are installed
- Optional node module dependencies are only checked if they are installed
- URL, Git URL, and file path versions are currently skipped

## Usage

### CLI
Run `check-installed` before npm scripts or directly to ensure that the environment is set up correctly.
If any check fails, the process will throw and exit with a non-zero exit code.

#### NPM Scripts
You can add a check script and run with `npm run check` or call it from other commands:
```json
  "scripts": {
    "check": "check-installed",
    "start": "npm run check && node index.js",
    "test": "npm run check && jest"
  }
```
This repo actually uses `check-installed` directly 🤯.

#### Command Line
```bash
npx check-installed
```

### API
You can call the checks directly from node programatically if needed like:
```js
const { checkEngines, checkModules } = require('check-installed')
const json = require('./package.json')

await checkEngines(json)
await checkModules(json)
await checkEngines(json, { showEngines: true })
await checkModules(json, { showSuccess: true })
```
Each check returns a `Promise<void>` that throws if any check fails.
They are typed to accept the entire `options` object, but only use options they need.

### Options
- All options are optional and will be defaulted
- CLI formats to change default values:
  - `boolean`:
    - `true`: `--option-name` or `--option-name=true`
    - `false`: `--option-name=false` or `--option-name=` or `--option-name=anythingelse`
  - `string`: `--option-name=value`
- Options for `checkEngines` and `checkModules` are the same as CLI options but `camelCased`

#### `--help`
Show help (skips check)

*Type: `boolean`, Default: `false`*

#### `--skip-engines`
Skip checking engines

*Type: `boolean`, Default: `false`*

#### `--skip-modules`
Skip checking node modules

*Type: `boolean`, Default: `false`*

#### `--show-success`
Show success message on successful checks

*Type: `boolean`, Default: `false`*

#### `--show-engines`
Show installed engines on successful check

*Type: `boolean`, Default: `false`*

#### `--show-modules`
Show installed node modules on successful check

*Type: `boolean`, Default: `false`*
